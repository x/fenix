=====
Fenix
=====

OpenStack host maintenance and upgrade in interaction with the application

Fenix implements rolling infrastructure maintenance and upgrade in interaction
with the application on top of it. In Telco world we talk about VNFM, but one can
implement his own simple manager for any application.

Infrastructure admin can call Fenix API to start a maintenance workflow
session. This session will make needed maintenance and upgrade operations to
infrastructure in interaction with the application manager to guarantee zero
downtime for its service. Interaction gives the ability for the application manager
to know about new capabilities coming over maintenance to make his own upgrade.
The application can have a time window to finish what he is doing, make own action
to re-instantiate his instance or have Fenix to make the migration. Also scaling
applications or retirement will be possible.

As Fenix has project-specific messaging with information about instances
affected towards the application manager, it will also have admin-level messaging.
This messaging can tell what host is down for maintenance, back in use, added
or retired. Any infrastructure component can catch this information as needed.

Fenix also works with "one-click". Infrastructure admin just creates the
workflow session he wants and all needed software changes are automatically
downloaded, the workflow is run to wanted hosts according to the request and
depending on how the used workflow plug-in and action plug-ins are
implemented.

In the NFV Fenix needs to be supported by infrastructure admin UI, VNFM and
VNF implementation. Fenix itself should be integrated into infrastructure
to be used it the infrastructure maintenance, upgrade, scaling and life-cycle
operations.

* Free software: Apache license
* Documentation: https://fenix.readthedocs.io/en/latest/index.html
* Developer Documentation: https://wiki.openstack.org/wiki/Fenix
* Source: https://opendev.org/x/fenix
* Running sample workflows: https://opendev.org/x/fenix/src/branch/master/fenix/tools/README.md
* Bug tracking and Blueprints: https://storyboard.openstack.org/#!/project/x/fenix
* How to contribute: https://docs.openstack.org/infra/manual/developers.html
* `Fenix Specifications <specifications/index.html>`_
