# Copyright (c) 2018 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import gettext
from wsgiref import simple_server

from oslo_config import cfg
from oslo_log import log as logging

gettext.install('fenix')

from fenix.api.v1 import app as v1_app

LOG = logging.getLogger(__name__)
CONF = cfg.CONF


def main():
    logging.setup(cfg.CONF, 'fenix')
    app = v1_app.make_app()
    srv = simple_server.make_server(cfg.CONF.host, cfg.CONF.port, app)
    LOG.info("fenix-api started")
    srv.serve_forever()


if __name__ == "__main__":
    main()
