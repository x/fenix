# Copyright (c) 2019 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import os
import requests
import tarfile


SUPPORTED_PACKAGE = {'.tar.gz': 'r:gz', '.tar.bz2': 'r:bz2', '.tar': 'r:'}


def check_url(url):
    h = requests.head(url, allow_redirects=True)
    header = h.headers
    content_type = header.get('content-type')
    if any(ft in content_type.lower() for ft in ['text', 'html']):
        return False
    return True


def url_to_filename(target_dir, url):
    if url.find('/'):
        return target_dir + '/' + url.rsplit('/', 1)[1]
    else:
        return None


def extract_plugin(target_dir, fname):
    dest_dir = None
    if "workflow" in fname.lower():
        dest_dir = target_dir + '/workflow'
    elif "action" in fname.lower():
        dest_dir = target_dir + '/actions'
    if dest_dir is not None:
        if not os.path.isdir(dest_dir):
            os.mkdir(dest_dir)
        for postfix in SUPPORTED_PACKAGE:
            if (fname.endswith(postfix)):
                tar = tarfile.open(fname, SUPPORTED_PACKAGE[postfix])
                tar.extractall(path=dest_dir)
                tar.close()
                break


def download_url(target_dir, url):
    if not check_url(url):
        raise Exception("%s is not a downloadable file" % url)
    fname = url_to_filename(target_dir, url)
    if fname is None:
        raise Exception("Cannot make filename from: %s " % url)
    r = requests.get(url, allow_redirects=True)
    if not os.path.isdir(target_dir):
        os.mkdir(target_dir)
    open(fname, 'wb').write(r.content)
    extract_plugin(target_dir, fname)
