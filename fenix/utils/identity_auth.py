# Copyright (c) 2018 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import os

from keystoneauth1 import loading
from keystoneauth1 import session
from oslo_config import cfg


os_opts = [
    cfg.StrOpt('os_auth_url',
               default=os.environ.get('OS_AUTH_URL', ''),
               help='OpenStack Identity service URL.'),
    cfg.StrOpt('os_username',
               default=os.environ.get('OS_USERNAME', 'fenix'),
               help='Fenix user. Must have admin role'),
    cfg.StrOpt('os_password',
               default=os.environ.get('OS_PASSWORD', 'fenix'),
               help='Fenix admin user password'),
    cfg.StrOpt('os_user_domain_name',
               default=os.environ.get('OS_USER_DOMAIN_NAME', 'default'),
               help='A domain name the os_username belongs to.'),
    cfg.StrOpt('os_project_name',
               default=os.environ.get('OS_PROJECT_NAME',
                                      os.environ.get('OS_TENANT_NAME')),
               help='the Fenix admin project'),
    cfg.StrOpt('os_project_domain_name',
               default=os.environ.get('OS_PROJECT_DOMAIN_NAME', 'default'),
               help='the Fenix admin project domain')
]


def get_identity_auth(conf, project=None):
    loader = loading.get_plugin_loader('password')
    return loader.load_from_options(
        auth_url=conf.service_user.os_auth_url,
        username=conf.service_user.os_username,
        password=conf.service_user.os_password,
        user_domain_name=conf.service_user.os_user_domain_name,
        project_name = (project or conf.service_user.os_project_name),
        tenant_name=conf.service_user.os_project_name,
        project_domain_name=conf.service_user.os_project_domain_name)


def get_session(auth=None):
    """Get a user credentials auth session."""
    if auth is None:
        auth = get_identity_auth()
    return session.Session(auth=auth,
                           verify=os.environ.get('OS_CACERT'))
