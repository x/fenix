#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_policy import policy

from fenix.policies import base

POLICY_ROOT = 'fenix:maintenance:session:%s'

session_policies = [
    policy.DocumentedRuleDefault(
        name=POLICY_ROOT % 'get',
        check_str=base.RULE_ADMIN,
        description='Policy rule for showing maintenance session API',
        operations=[
            {
                'path': '/{api_version}/maintenance/{seission_id}',
                'method': 'GET'
            }
        ]
    ),
    policy.DocumentedRuleDefault(
        name=POLICY_ROOT % 'put',
        check_str=base.RULE_ADMIN,
        description='Policy rule for updating maintenance session API',
        operations=[
            {
                'path': '/{api_version}/maintenance/{seission_id}',
                'method': 'PUT'
            }
        ]
    ),
    policy.DocumentedRuleDefault(
        name=POLICY_ROOT % 'delete',
        check_str=base.RULE_ADMIN,
        description='Policy rule for deleting maintenance session API',
        operations=[
            {
                'path': '/{api_version}/maintenance/{seission_id}',
                'method': 'DELETE'
            }
        ]
    )
]


def list_rules():
    return session_policies
