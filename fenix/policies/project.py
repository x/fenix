#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_policy import policy

from fenix.policies import base

POLICY_ROOT = 'fenix:maintenance:session:project:%s'

project_policies = [
    policy.DocumentedRuleDefault(
        name=POLICY_ROOT % 'get',
        check_str=base.RULE_ADMIN_OR_OWNER,
        description='Policy rule for showing project session affected '
                    'instances API.',
        operations=[
            {
                'path': '/{api_version}/maintenance/{seission_id}/'
                '{project_id}',
                'method': 'GET'
            }
        ]
    ),
    policy.DocumentedRuleDefault(
        name=POLICY_ROOT % 'put',
        check_str=base.RULE_ADMIN_OR_OWNER,
        description='Policy rule for setting project session affected '
                    'instances actions API.',
        operations=[
            {
                'path': '/{api_version}/maintenance/{seission_id}/'
                '{project_id}',
                'method': 'PUT'
            }
        ]
    )
]


def list_rules():
    return project_policies
