# Copyright (c) 2013 Mirantis Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Defines interface for DB access.

Functions in this module are imported into the fenix.db namespace. Call these
functions from fenix.db namespace, not the fenix.db.api namespace.

All functions in this module return objects that implement a dictionary-like
interface.

**Related Flags**

:db_backend:  string to lookup in the list of LazyPluggable backends.
              `sqlalchemy` is the only supported backend right now.

:sql_connection:  string specifying the sqlalchemy connection to use, like:
                  `sqlite:///var/lib/fenix/fenix.sqlite`.

"""

from oslo_config import cfg
from oslo_db import api as db_api
from oslo_db import options as db_options
from oslo_log import log as logging


_BACKEND_MAPPING = {
    'sqlalchemy': 'fenix.db.sqlalchemy.api',
}

db_options.set_defaults(cfg.CONF)
IMPL = db_api.DBAPI(cfg.CONF.database.backend,
                    backend_mapping=_BACKEND_MAPPING)
LOG = logging.getLogger(__name__)


def get_instance():
    """Return a DB API instance."""
    return IMPL


def setup_db():
    """Set up database, create tables, etc.

    Return True on success, False otherwise
    """
    return IMPL.setup_db()


def drop_db():
    """Drop database.

    Return True on success, False otherwise
    """
    return IMPL.drop_db()


# Helpers for building constraints / equality checks


def constraint(**conditions):
    """Return a constraint object suitable for use with some updates."""
    return IMPL.constraint(**conditions)


def equal_any(*values):
    """Return an equality condition object suitable for use in a constraint.

    Equal_any conditions require that a model object's attribute equal any
    one of the given values.
    """
    return IMPL.equal_any(*values)


def not_equal(*values):
    """Return an inequality condition object suitable for use in a constraint.

    Not_equal conditions require that a model object's attribute differs from
    all of the given values.
    """
    return IMPL.not_equal(*values)


def to_dict(func):
    def decorator(*args, **kwargs):
        res = func(*args, **kwargs)

        if isinstance(res, list):
            return [item.to_dict() for item in res]

        if res:
            return res.to_dict()
        else:
            return None

    return decorator


# Fenix workflow session DB access
def create_session(values):
    """Create a session from the values."""
    return IMPL.create_session(values)


def update_session(values):
    return IMPL.update_session(values)


def remove_session(session_id):
    """Remove a session from the tables."""
    return IMPL.remove_session(session_id)


def get_session(session_id):
    return IMPL.maintenance_session_get(session_id)


def get_sessions():
    return IMPL.maintenance_session_get_all()


def create_action_plugin(values):
    """Create a action from the values."""
    return IMPL.create_action_plugin(values)


def create_action_plugins(session_id, action_dict_list):
    return IMPL.create_action_plugins(action_dict_list)


def get_action_plugins(session_id):
    return IMPL.action_plugins_get_all(session_id)


def create_action_plugin_instance(values):
    return IMPL.create_action_plugin_instance(values)


def get_action_plugin_instances(session_id):
    return IMPL.action_plugin_instances_get_all(session_id)


def update_action_plugin_instance(values):
    return IMPL.update_action_plugin_instance(values)


def remove_action_plugin_instance(ap_instance):
    return IMPL.remove_action_plugin_instance(ap_instance)


def create_downloads(download_dict_list):
    return IMPL.create_downloads(download_dict_list)


def get_downloads(session_id):
    return IMPL.download_get_all(session_id)


def create_host(values):
    """Create a host from the values."""
    return IMPL.create_host(values)


def update_host(values):
    return IMPL.update_host(values)


def create_hosts(session_id, hostnames):
    hosts = []
    for hostname in hostnames:
        host = {
            'session_id': session_id,
            'hostname': str(hostname),
            'type': None,
            'maintained': False,
            'disabled': False}
        hosts.append(host)
    return IMPL.create_hosts(hosts)


def create_hosts_by_details(session_id, hosts_dict_list):
    hosts = []
    for hd in hosts_dict_list:
        host = {}
        host['session_id'] = session_id
        host['hostname'] = hd["hostname"]
        host['type'] = hd["type"]
        host['maintained'] = hd['maintained']
        host['disabled'] = hd['disabled']
        if 'details' in hd:
            host['details'] = hd['details']
        hosts.append(host)
    return IMPL.create_hosts(hosts)


def get_hosts(session_id):
    return IMPL.hosts_get(session_id)


def create_projects(session_id, project_ids):
    projects = []
    for project_id in project_ids:
        project = {
            'session_id': session_id,
            'project_id': str(project_id),
            'state': None}
        projects.append(project)
    return IMPL.create_projects(projects)


def update_project(values):
    return IMPL.update_project(values)


def get_projects(session_id):
    return IMPL.projects_get(session_id)


def update_instance(values):
    return IMPL.update_instance(values)


def create_instance(values):
    """Create a instance from the values."""
    return IMPL.create_instance(values)


def create_instances(instances):
    """Create a instances from the instances dictionary list."""
    return IMPL.create_instances(instances)


def remove_instance(session_id, instance_id):
    return IMPL.remove_instance(session_id, instance_id)


def get_instances(session_id):
    return IMPL.instances_get(session_id)


def update_project_instance(values):
    return IMPL.update_project_instance(values)


def remove_project_instance(instance_id):
    return IMPL.remove_project_instance(instance_id)


def project_instance_get(instance_id):
    return IMPL.project_instance_get(instance_id)


def group_instances_get(group_id):
    return IMPL.group_instances_get(group_id)


def instance_group_get(group_id):
    return IMPL.instance_group_get(group_id)


def instance_group_get_detailed(group_id):
    instances = group_instances_get(group_id)
    group = instance_group_get(group_id)
    group['instance_ids'] = instances
    return group


def update_instance_group(values):
    return IMPL.update_instance_group(values)


def remove_instance_group(instance_id):
    return IMPL.remove_instance_group(instance_id)
