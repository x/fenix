# Copyright (c) 2019 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""Version 1 of the API.
"""

from oslo_log import log as logging
import pecan
from pecan import rest

from fenix.api.v1.controllers import maintenance as controller

LOG = logging.getLogger(__name__)


class V1Controller(rest.RestController):

    versions = [{"id": "v1", "status": "CURRENT"}]
    _routes = {}

    # maps to v1/maintenance
    maintenance = controller.MaintenanceController()
    # maps to v1/maintenance/{session_id}
    session = controller.SessionController()
    # maps to v1/maintenance/{session_id}/{project_id}
    project = controller.ProjectController()
    # maps to v1/maintenance/{session_id}/{project_id}/{instance_id}
    project_instance = controller.ProjectInstanceController()
    # maps to v1/instance/{instance_id}
    instance = controller.InstanceController()
    # maps to v1/instance_group/{group_id}
    instance_group = controller.InstanceGroupController()

    @pecan.expose()
    def _route(self, args):
        """Overrides the default routing behavior.

        It allows to map controller URL with correct controller instance.
        By default, it maps with the same name.
        """
        try:
            route = self._routes.get(args[0], args[0])
            depth = len(args)
            if route is None:
                # NOTE(sbauza): Route must map to a non-existing controller
                args[0] = 'http404-nonexistingcontroller'
            elif depth == 1:
                args[0] = route
            elif depth == 2:
                if route == "maintenance":
                    args[0] = "session"
                elif route in ["instance", "instance_group"]:
                    args[0] = route
                else:
                    args[0] = 'http404-nonexistingcontroller'
            elif depth == 3 and route == "maintenance":
                last = self._routes.get(args[2], args[2])
                if last == "detail":
                    args[0] = "session"
                else:
                    args[0] = "project"
            elif depth == 4 and route == "maintenance":
                args[0] = "project_instance"
            else:
                args[0] = 'http404-nonexistingcontroller'
        except IndexError:
            LOG.error("No args found on V1 controller")
        return super(V1Controller, self)._route(args)
