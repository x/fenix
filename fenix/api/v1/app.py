# Copyright (c) 2018 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import sys

from keystonemiddleware import auth_token
from oslo_config import cfg
from oslo_log import log as logging
import pecan

from fenix.api.v1 import hooks
from fenix.api.v1 import middleware

LOG = logging.getLogger(__name__)

api_opts = [
    cfg.StrOpt('api_config',
               default="api.conf",
               help="Configuration file for API service."),
    cfg.StrOpt('host',
               default="127.0.0.1",
               help="API host IP"),
    cfg.IntOpt('port',
               default=5000,
               help="API port to use.")
]

CONF = cfg.CONF
CONF.register_opts(api_opts)
logging.register_options(cfg.CONF)
cfg.CONF(sys.argv[1:], project='fenix', prog='fenix-api')


def setup_app(pecan_config=None, debug=False, argv=None):

    app_hooks = [hooks.ConfigHook(),
                 hooks.DBHook(),
                 hooks.ContextHook(),
                 hooks.RPCHook()]
    app = pecan.make_app(pecan_config.app.root,
                         debug=True,
                         hooks=app_hooks,
                         wrap_app=middleware.ParsableErrorMiddleware,
                         guess_content_type_from_ext=False)

    # WSGI middleware for debugging
    # if CONF.log_exchange:
    #     app = debug.Debug.factory(pecan_config)(app)

    # WSGI middleware for Keystone auth
    # NOTE(sbauza): ACLs are always active unless for unittesting where
    #               enable_acl could be set to False
    if pecan_config.app.enable_acl:
        app = auth_token.AuthProtocol(app, {})

    return app


def make_app():
    config = {
        'app': {
            'modules': ['fenix.api.v1'],
            'root': 'fenix.api.root.RootController',
            'enable_acl': True,
        }
    }
    app = pecan.load_app(config)
    return app
