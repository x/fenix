# Copyright 2020 OpenStack Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

uid = {
    'type': 'string',
    'minLength': 8,
    'maxLength': 36,
}

states = ['MAINTENANCE',
          'SCALE_IN',
          'PREPARE_MAINTENANCE',
          'START_MAINTENANCE',
          'PLANNED_MAINTENANCE',
          'MAINTENANCE_COMPLETE',
          'MAINTENANCE_DONE',
          'MAINTENANCE_FAILED']

reply_states = ['ACK_MAINTENANCE',
                'ACK_SCALE_IN',
                'ACK_PREPARE_MAINTENANCE',
                'ACK_START_MAINTENANCE',
                'ACK_PLANNED_MAINTENANCE',
                'ACK_MAINTENANCE_COMPLETE',
                'NACK_MAINTENANCE',
                'NACK_SCALE_IN',
                'NACK_PREPARE_MAINTENANCE',
                'NACK_START_MAINTENANCE',
                'NACK_PLANNED_MAINTENANCE',
                'NACK_MAINTENANCE_COMPLETE']

allowed_actions = ['MIGRATE', 'LIVE_MIGRATE', 'OWN_ACTION', 'EVICTION']

maintenance_session_project_put = {
    'type': 'object',
    'properties': {
        'instance_actions': {
            'type': 'object'
        },
        'state': {
            'type': 'string',
            'enum': reply_states,
        },
    },
    'required': ['state']
}

maintenance_session_project_instance_put = {
    'type': 'object',
    'properties': {
        'instance_action': {
            'type': 'string',
            'enum': allowed_actions,
        },
        'state': {
            'type': 'string',
            'enum': reply_states,
        }
    },
    'required': ['instance_action', 'state']
}


maintenance_session_put = {
    'type': 'object',
    'properties': {
    'state': {
            'type': 'string',
            'enum': states,
        }
    }
}

maintenance_post = {
    'type': 'object',
    'properties': {
        'hosts': {
            'type': 'array',
            'minItems': 0,
            'maxItems': 1000,
            'items': {
                'type': 'string',
                'minLength': 2,
                'maxLength': 255,
            },
        },
        'state': {
            'type': 'string',
            'enum': states,
        },
        'maintenance_at': {
            'type': 'string',
            'format': 'date-time',
        },
        'metadata': {'type': 'object'},
        'workflow': {
            'type': 'string',
            'minLength': 2,
            'maxLength': 255,
        },
        'download': {
            'type': 'array',
            'minItems': 5,
            'maxItems': 255,
            'items': {
                'type': 'string',
                'minLength': 2,
                'maxLength': 165,
            },
        },
        'actions': {
            'type': 'array',
            'minItems': 0,
            'maxItems': 255,
            'items': {
                'type': 'object',
                'properties': {
                    'plugin': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 255,
                    },
                    'type': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 32,
                    },
                    'metadata': {'type': 'object'},
                },
                'required': ['plugin', 'type']
            }
        }
    },
    'required': ['state', 'maintenance_at', 'workflow', 'metadata']
}

instance_put = {
    'type': 'object',
    'properties': {
        'instance_id': uid,
        'project_id': uid,
        'group_id': uid,
        'instance_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255,
        },
        'max_interruption_time': {
            'type': 'number',
            'maximum': 21600
        },
        'migration_type': {
            'type': 'string',
            'enum': allowed_actions,
        },
        'resource_mitigation': {'type': 'boolean'},
        'lead_time': {
            'type': 'number',
            'maximum': 21600
        },
    },
    'required': ['instance_id', 'project_id', 'group_id', 'instance_name',
                 'max_interruption_time', 'migration_type',
                 'resource_mitigation', 'lead_time']
}

instance_group_put = {
    'type': 'object',
    'properties': {
        'project_id': uid,
        'group_id': uid,
        'group_name': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 255,
        },
        'anti_affinity_group': {'type': 'boolean'},
        'max_instances_per_host': {
            'type': 'number',
            'maximum': 32000
        },
        'max_impacted_members': {
            'type': 'number',
            'minimum': 1,
            'maximum': 32000
        },
        'recovery_time': {
            'type': 'number',
            'maximum': 21600
        },
        'resource_mitigation': {'type': 'boolean'},
    },
    'required': ['project_id', 'group_id', 'group_name',
                 'anti_affinity_group', 'max_instances_per_host',
                 'max_impacted_members', 'recovery_time',
                 'resource_mitigation']
}
