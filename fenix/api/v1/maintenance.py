# Copyright (c) 2019 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_log import log

from fenix import engine
from fenix.utils import service

LOG = log.getLogger(__name__)


class EngineRPCAPI(service.RPCClient):
    BASE_RPC_API_VERSION = '1.0'

    def __init__(self):
        """Initiate RPC API client with needed topic and RPC version."""
        super(EngineRPCAPI, self).__init__(engine.get_target())

    def admin_get(self):
        """Get maintenance workflow sessions"""
        return self.call('admin_get')

    def admin_create_session(self, data):
        """Create maintenance workflow session thread"""
        return self.call('admin_create_session', data=data)

    def admin_get_session(self, session_id):
        """Get maintenance workflow session state"""
        return self.call('admin_get_session', session_id=session_id)

    def admin_get_session_detail(self, session_id):
        """Get maintenance workflow session details"""
        return self.call('admin_get_session_detail', session_id=session_id)

    def admin_delete_session(self, session_id):
        """Delete maintenance workflow session thread"""
        return self.call('admin_delete_session', session_id=session_id)

    def admin_update_session(self, session_id, data):
        """Update maintenance workflow session"""
        return self.call('admin_update_session', session_id=session_id,
                         data=data)

    def project_get_session(self, session_id, project_id):
        """Get maintenance workflow session project specific details"""
        return self.call('project_get_session', session_id=session_id,
                         project_id=project_id)

    def project_update_session(self, session_id, project_id, data):
        """Update maintenance workflow session project state"""
        return self.call('project_update_session', session_id=session_id,
                         project_id=project_id, data=data)

    def project_update_session_instance(self, session_id, project_id,
                                        instance_id, data):
        """Update maintenance workflow session project instance project_state

        """
        return self.call('project_update_session_instance',
                         session_id=session_id,
                         project_id=project_id,
                         instance_id=instance_id,
                         data=data)

    def get_instance(self, instance_id):
        """Get internal instance"""
        return self.call('get_instance', instance_id=instance_id)

    def update_instance(self, instance_id, data):
        """Update internal instance"""
        return self.call('update_instance', instance_id=instance_id,
                         data=data)

    def delete_instance(self, instance_id):
        """Delete internal instance"""
        return self.call('delete_instance', instance_id=instance_id)

    def get_instance_group(self, group_id):
        """Get internal instance group"""
        return self.call('get_instance_group', group_id=group_id)

    def update_instance_group(self, group_id, data):
        """Update internal instance group"""
        return self.call('update_instance_group', group_id=group_id,
                         data=data)

    def delete_instance_group(self, group_id):
        """Delete internal instance group"""
        return self.call('delete_instance_group', group_id=group_id)
