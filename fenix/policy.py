# Copyright (c) 2013 Bull.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Policy Engine For Fenix."""

import functools

from oslo_config import cfg
from oslo_log import log as logging
from oslo_policy import opts
from oslo_policy import policy

from fenix import context
from fenix import exceptions
from fenix import policies

CONF = cfg.CONF
opts.set_defaults(CONF)
LOG = logging.getLogger(__name__)

_ENFORCER = None


def reset():
    global _ENFORCER

    if _ENFORCER:
        _ENFORCER.clear()
    _ENFORCER = None


def init():
    global _ENFORCER
    if not _ENFORCER:
        _ENFORCER = policy.Enforcer(CONF)
        _ENFORCER.register_defaults(policies.list_rules())


def set_rules(data, default_rule=None):
    default_rule = default_rule or CONF.policy_default_rule
    if not _ENFORCER:
        init()
    if default_rule:
        _ENFORCER.default_rule = default_rule
    _ENFORCER.set_rules(policy.Rules.load(data, default_rule))


def enforce(context, action, target, do_raise=True):
    """Verifies that the action is valid on the target in this context.

       :param context: fenix context
       :param action: string representing the action to be checked
       :param target: dictionary representing the object of the action
           for object creation this should be a dictionary representing the
           location of the object e.g. ``{'project_id': context.project_id}``
       :param do_raise: if True (the default), raises PolicyNotAuthorized;
           if False, returns False
       :raises fenix.exceptions.PolicyNotAuthorized: if verification fails
           and do_raise is True.
       :return: returns a non-False value (not necessarily "True") if
           authorized, and the exact value False if not authorized and
           do_raise is False.
    """

    init()

    credentials = context.to_dict()
    LOG.debug("enforce %s" % credentials)

    # Add the exceptions arguments if asked to do a raise
    extra = {}
    if do_raise:
        extra.update(exc=exceptions.PolicyNotAuthorized, action=action)

    return _ENFORCER.enforce(action, target, credentials, do_raise=do_raise,
                             **extra)


def authorize(extension, action=None, api='fenix', ctx=None,
              target=None):
    def decorator(func):

        @functools.wraps(func)
        def wrapped(self, *args, **kwargs):
            cur_ctx = ctx or context.current()
            tgt = target or {'project_id': cur_ctx.project_id,
                             'user_id': cur_ctx.user_id}
            LOG.debug("authorize target: %s" % tgt)
            if action is None:
                act = '%s:%s' % (api, extension)
            else:
                act = '%s:%s:%s' % (api, extension, action)
            LOG.debug("authorize policy: %s" % act)
            enforce(cur_ctx, act, tgt)
            try:
                return func(self, *args, **kwargs)
            except TypeError:
                # TBD Invalid Method should always be caught before authorize
                # This makes sure we get some feasible error
                raise exceptions.NotFound(object=str(act))

        return wrapped
    return decorator
