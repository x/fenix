==========
User guide
==========

.. toctree::
   :maxdepth: 2

   architecture
   baseworkflow
   advanced_workflow
   notifications
