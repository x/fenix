.. _install:

Install and configure
~~~~~~~~~~~~~~~~~~~~~

This section describes how to install and configure the
host maintenance service, code-named Fenix, on the controller node.

This section assumes that you already have a working OpenStack
environment.

Note that installation and configuration vary by distribution.
Currently Fenix is not included in any distributions. Instead
there is shown the generic way of installing and how to install
via DevStack.

.. toctree::
   :maxdepth: 2

   install-rdo.rst
   install-ubuntu.rst
   install-obs.rst
   install-devstack.rst
