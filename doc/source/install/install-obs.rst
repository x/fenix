.. _install-obs:


Install and configure for openSUSE and SUSE Linux Enterprise
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section describes how to install and configure the Fenix service
for openSUSE and SUSE Linux Enterprise Server.

.. include:: common_prerequisites.rst

.. include:: ../configuration/configuration.rst

Finalize installation
---------------------

Start the fenix services and configure them to start when
the system boots:

.. code-block:: console

   # sudo systemctl enable openstack-fenix-api.service
   # sudo systemctl start openstack-fenix-api.service

   # sudo systemctl enable openstack-fenix-engine.service
   # sudo systemctl start openstack-fenix-engine.service
