.. _verify:

Verify operation
~~~~~~~~~~~~~~~~

Verify operation of the fenix service.

.. note::

   Perform these commands on the controller node.

#. List service components to verify successful launch and registration
   of each process. Example for DevStack:

   .. code-block:: console

      $ sudo systemctl status devstack@fenix*
