.. _install-devstack:

Install and configure for DevStack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install and configure components
--------------------------------

#. Install Fenix by adding needed options to local.conf

   .. code-block:: ini

      #Enable Fenix plugin
      enable_plugin fenix https://opendev.org/x/fenix

      #Enable Fenix services
      enable_service fenix-engine
      enable_service fenix-api
