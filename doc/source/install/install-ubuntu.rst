.. _install-ubuntu:

Install and configure for Ubuntu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section describes how to install and configure the Fenix
service for Ubuntu.

.. include:: common_prerequisites.rst

.. include::  ../configuration/configuration.rst

Finalize installation
---------------------

Restart the fenix services:

.. code-block:: console

   # sudo service openstack-fenix-api restart
   # sudo service openstack-fenix-engine restart
