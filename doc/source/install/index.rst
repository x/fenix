================================
Fenix service installation guide
================================

.. toctree::
   :maxdepth: 2

   get_started.rst
   install.rst
   verify.rst

The host maintenance service (Fenix)  provides...

This chapter assumes a working setup of OpenStack following the
`OpenStack Installation Tutorial
<https://docs.openstack.org/install-guide/>`_.
