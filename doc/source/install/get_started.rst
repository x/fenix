======================
Fenix service overview
======================
The Fenix service provides...

The Fenix service consists of the following components:

``fenix-api`` service
  Accepts and responds to end user API calls

``fenix-engine`` service
  Runs the pluggable maintenance sessions
