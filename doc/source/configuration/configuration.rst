Configuration files
-------------------

Configuration options. All options have default values. Mandatory options are
mentioned as those are usually at least the ones needed to be defined to match
to the current system.

   * Edit the ``/etc/fenix/fenix-api.conf`` file the configure fenix-api

     .. code-block:: ini

        [DEFAULT]

        # Mandatory configuration options

        # Host where API is running. default="127.0.0.1"
        host = <hostname>
        # API Port. default=5000
        port = <port>
        # An URL representing the messaging driver to use and its full configuration.
        transport_url = <transport URL>

        [keystone_authtoken]
        # OpenStack Identity service URL.
        auth_url = http://127.0.0.1/identity
        # Authentication type
        auth_type = password
        # PEM encoded Certificate Authority to use when verifying HTTPs connections.
        cafile = /opt/stack/data/ca-bundle.pem
        # The Fenix admin project domain.
        project_domain_name = Default
        # The Fenix admin project.
        project_name = admin
        # A domain name the os_username belongs to.
        user_domain_name = Default
        # Fenix admin user password.
        password = admin
        # Fenix user. Must have admin role.
        username = admin


   * Edit the ``/etc/fenix/fenix.conf`` file the configure fenix-engine

     .. code-block:: ini

        [DEFAULT]

        # Mandatory configuration options

        # Host where engine is running. default="127.0.0.1"
        host = <hostname>
        # API Port. default=5000
        port = <port>
        # An URL representing the messaging driver to use and its full configuration.
        transport_url = <transport URL>

        # Optional configuration options

        # Wait for project reply after message sent to project. default 120
        wait_project_reply = 120
        # Project maintenance reply confirmation time in seconds. default 40
        project_maintenance_reply = 40
        # Project scale in reply confirmation time in seconds. default 60
        project_scale_in_reply = 60
        # Number of live migration retries. default 5
        live_migration_retries = 5
        # How long to wait live migration to be done. default 600
        live_migration_wait_time = 600

        [database]

        # database connection URL
        connection = mysql+pymysql://fenix:FENIX_DBPASS@controller/fenix

        [service_user]

        # OpenStack Identity service URL. Default to environmental variable OS_AUTH_URL
        os_auth_url = http://127.0.0.1/identity
        # Fenix user. Must have admin role. Default to environmental variable OS_USERNAME
        os_username = admin
        # Fenix admin user password. Default to environmental variable OS_PASSWORD
        os_password = admin
        # A domain name the os_username belongs to. Default to environmental variable OS_USER_DOMAIN_NAME
        os_user_domain_name = default
        # The Fenix admin project. Default to environmental variable OS_PROJECT_NAME
        os_project_name = admin
        # The Fenix admin project domain. Default to environmental variable OS_PROJECT_DOMAIN_NAME
        os_project_domain_name = default
