=============
Configuration
=============

.. toctree::
   :maxdepth: 2

   configuration.rst

Dependencies and special configuration
======================================

Fenix Default workflow VNFM interaction also assumes AODH is installed.
Among that, here is mentioned what you may want to configure when using Fenix.

.. toctree::
   :maxdepth: 2

   ../configuration/dependencies.rst
