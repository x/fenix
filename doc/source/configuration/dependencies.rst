.. _dependencies:

Fenix external dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nova
----

Fenix will normally use cold and live migrations. For these to work, Nova
service user should be configured to be able to ssh between compute nodes.
You may also want to change some other related configuration parameters.

AODH and Ceilometer configuration
---------------------------------

When want to utilize the VNF(M)/EM interaction with Fenix, VNF needs to
supbscribe to AODH event alarm for 'maintenance.scheduled' type of
notifications.

Any service may also want to know when host is added, retired, in maintenance
or back from maintenance. For this those services can subscribe to AODH event
alarm for 'maintenance.host' type of notification.

  * /etc/ceilometer/event_definitions.yaml

     .. code-block:: yaml

      - event_type: maintenance.scheduled
          traits:
            actions_at:
              fields: payload.maintenance_at
              type: datetime
            allowed_actions:
              fields: payload.allowed_actions
            host_id:
              fields: payload.host_id
            instances:
              fields: payload.instances
            metadata:
              fields: payload.metadata
            project_id:
              fields: payload.project_id
            reply_url:
              fields: payload.reply_url
            session_id:
              fields: payload.session_id
            state:
              fields: payload.state
      - event_type: maintenance.host
          traits:
            host:
              fields: payload.host
            project_id:
              fields: payload.project_id
            session_id:
              fields: payload.session_id
            state:
              fields: payload.state

  * /etc/ceilometer/event_pipeline.yaml

    .. code-block:: yaml

        - notifier://
        - notifier://?topic=alarm.all

For AODH and Ceilometer configuration to take into effect, you may want to
restart corresponding services

   .. code-block:: console

    $ sudo systemctl restart openstack-aodh-listener.service
    $ sudo systemctl restart openstack-ceilometer-notification.service

In DevStack you may want to enable Ceilometer and AODH in local.conf

   .. code-block:: ini

    enable_plugin ceilometer https://opendev.org/openstack/ceilometer
    enable_plugin aodh https://opendev.org/openstack/aodh
