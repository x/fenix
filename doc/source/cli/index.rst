================================
Command line interface reference
================================

CLI reference of Fenix.

Currently, Fenix does not implement CLI. Real product integration is
expected to have GUI and VNFM (application manager) to support Fenix.
In the OPNFV Doctor, the maintenance test case implements infrastructure
admin and VNFM behavior. In Fenix, you can find 'infra_admin.py' and
'vnfm.py' that implements the same and are always up-to-date to be tested
against Fenix example workflows. Those are anyhow just for the testing sample
application but works to get the idea for product implementation.
