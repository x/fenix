====================
Administrators guide
====================

Fenix
=====

Fenix should be deployed to the infrastructure controller or manager node
and be used for any cloud infrastructure maintenance, upgrade, scaling or
life-cycle operations.

VNF and VNFM
============

In the NFV use case, the VNF and VNFM need to support Fenix to optimize the
workflows and to guarantee zero impact to VNF during different infrastructure
operations. This means instance and instance group constraints and the
interaction with Fenix. Operations are most optimal when the VNF can be scaled
according to its current utilization level. This allows for the smallest possible
maintenance window, with zero impact on the VNF service.

Infrastructure Admin UI
=======================

Infrastructure admin UI needs to support Fenix. UI needs to be able to call
the Fenix admin APIs and preferably listen to Fenix admin events. UI should
be able to call different infrastructure maintenance and upgrade workflows
with needed parameters. APIs and events also give the possibility to have
detailed information about the workflow progress and to troubleshoot possible
errors. In complex clouds, errors can be still simple and quickly corrected
even manually. In this kind of special case, the UI can also support updating
Fenix workflow session to continue exactly where it failed. Explained in
`GET /v1/maintenance/{session_id}/detail`_

Integration
===========

The above-mentioned UI, VNF and VNFM are currently not in the scope of Fenix.
The implementation should be in other open-source projects or own proprietary
solutions. Currently, at least OpenStack Tacker is looking to support Fenix.

For testing the integration of Fenix, there is `tools`_ directory including
definitions of sample VNFs, VNFM and admin UI. There are also instructions on
how to test Fenix example workflows. These tools give an idea of what needs to
be supported and how to integrate Fenix in the production environment. Note
that tools only give what needs to be in the VNFM and VNF side for Fenix
testing purposes. They do not try to have a standard implementation like
VNFD or other needed interactions on VNF side. What is standard, is the
interfacing against Fenix.

.. _`GET /v1/maintenance/{session_id}/detail`: ../api-ref/v1/index.html?expanded=get-maintenance-session-details-detail#admin-workflow-session-api
.. _`tools`: https://opendev.org/x/fenix/src/branch/master/fenix/tools/README.md
