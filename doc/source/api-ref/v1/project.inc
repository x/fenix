.. -*- rst -*-

============================
Project workflow session API
============================

These APIs are generic for any cloud as instance ID should be something that can
be matched to virtual machines or containers regardless of the cloud underneath.

Get project maintenance session
===============================

.. rest_method:: GET /v1/maintenance/{session_id}/{project_id}

Get project instances belonging to the current state of maintenance session.
the Project-manager receives an AODH event alarm telling about different
maintenance states. Event data field length is very limited, so instances cannot
be given as a list in the event. Instead, there will be an URL given to below
API to get a project-specific list of instances.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: uuid-path
    - project_id: uuid-path

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: project-maintenance-session-post

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Input from project to maintenance session
=========================================

.. rest_method:: PUT /v1/maintenance/{session_id}/{project_id}

Project having instances on top of the infrastructure handled by a maintenance
session might need to make own action for its instances on top of a host going
into maintenance next, or reply an admin action to be done. This is, as the host
can go down or even be removed and the instances should be then running safely
somewhere else. Project manager receives an AODH event alarm telling which
instances are affected and when the project is ready, it makes its own action or
replies back an action which needs the admin privileges.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: uuid-path
    - project_id: uuid-path
    - instance_actions: instance-actions
    - state: workflow-state-reply

.. literalinclude:: samples/input-from-project-to-maintenance-session-put.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

===========================
Project NFV constraints API
===========================

These APIs are for VNFs, VNMF and EM that are made to support ETSI defined
standard VIM interface for sophisticated interaction to optimize rolling
maintenance, upgrade, scaling and lifecycle management. These interface
enhancements guarantees zero impact to VNF service during these operations
and defining real time constraints for optimal operation performace.

Input from project instance to maintenance session
==================================================

.. rest_method:: PUT /v1/maintenance/{session_id}/{project_id}/{instance_id}

When using workflow utilizing ETSI constraints the 'state' 'PREPARE_MAINTENANCE'
and 'PLANNED_MAINTENANCE' notifications will be instance specific. This means
also the reply needs to be instance specific instead the project specific above.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: uuid-path
    - project_id: uuid-path
    - instance_id: uuid-path
    - instance_action: instance-action
    - state: workflow-state-reply

.. literalinclude:: samples/input-from-project-instance-to-maintenance-session-put.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Get instance constraints saved in Fenix DB
==========================================

.. rest_method:: GET /v1/instance/{instance_id}

Get instance constraints saved in Fenix DB. Initially this information
is coming from VNF(M) and needs to be syncronized to Fenix.

Request
-------

.. rest_parameters:: parameters.yaml

    - project_id: uuid
    - instance_id: uuid-path
    - instance_id: uuid
    - group_id: group-uuid
    - instance_name: instance-name
    - migration_type: migration-type
    - max_interruption_time: max-interruption-time
    - resource_mitigation: resource-mitigation
    - lead_time: lead-time

.. literalinclude:: samples/instance-constraints.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 404
    - 422
    - 500

Update instance constraints saved to Fenix DB
=============================================

.. rest_method:: PUT /v1/instance/{instance_id}

Update instance constraints to Fenix DB. Initially this information
is coming from VNF(M) and needs to be syncronized to Fenix.

Request
-------

.. rest_parameters:: parameters.yaml

    - project_id: uuid
    - instance_id: uuid-path
    - instance_id: uuid
    - group_id: group-uuid
    - instance_name: instance-name
    - migration_type: migration-type
    - max_interruption_time: max-interruption-time
    - resource_mitigation: resource-mitigation
    - lead_time: lead-time

.. literalinclude:: samples/instance-constraints.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Delete instance constraints from Fenix DB
=========================================

.. rest_method:: DELETE /v1/instance/{instance_id}

When instance is deleted, the constraints should also be deleted from
the Fenix DB. As Fenix is aware of existing instances, this could later be
enhanced so that Fenix houskeeping could take care of removing deleted
instances.

Request
-------

.. rest_parameters:: parameters.yaml

    - instance_id: uuid-path

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Get instance group constraints saved in Fenix DB
================================================

.. rest_method:: GET /v1/instance_group/{group_id}

Get instance group constraints saved in Fenix DB. Initially this information
is coming from VNF(M) and needs to be syncronized to Fenix.

Request
-------

.. rest_parameters:: parameters.yaml

    - group_id: group-uuid-path
    - group_id: group-uuid
    - project_id: uuid
    - instance_id: uuid-path
    - instance_id: uuid
    - group_name: instance-group
    - migration_type: migration-type
    - max_interruption_time: max-interruption-time
    - resource_mitigation: resource-mitigation
    - lead_time: lead-time

.. literalinclude:: samples/instance-group-constraints.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Update instance group constraints saved to Fenix DB
===================================================

.. rest_method:: PUT /v1/instance_group/{group_id}

Update instance group constraints to Fenix DB. Initially this information
is coming from VNF(M) and needs to be syncronized to Fenix.

Request
-------

.. rest_parameters:: parameters.yaml

    - group_id: group-uuid-path
    - group_id: group-uuid
    - project_id: uuid
    - group_name: instance-group
    - anti_affinity_group: boolean
    - max_instances_per_host: max-instances-per-host
    - max_impacted_members: max-impacted-members
    - recovery_time: recovery-time

    - resource_mitigation: resource-mitigation

.. literalinclude:: samples/instance-group-constraints.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Delete instance group constraints from Fenix DB
===============================================

.. rest_method:: DELETE /v1/instance_group/{group_id}

When instance group is deleted, the constraints should also be deleted from
the Fenix DB. As Fenix is aware of existing instances, this could later be
enhanced so that Fenix houskeeping could take care of removing deleted
instances.

Request
-------

.. rest_parameters:: parameters.yaml

    - group_id: group-uuid-path

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500



