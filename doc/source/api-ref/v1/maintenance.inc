.. -*- rst -*-

==========================
Admin workflow session API
==========================

Create maintenance session
==========================

.. rest_method:: POST /v1/maintenance

Create a new maintenance session. You can specify a list of 'hosts' to be
maintained or have an empty list to indicate those should be self-discovered.
You need to have an initial state for the workflow in 'state'. 'workflow'
indicates the name of a Python plug-in to be used in the maintenance.

Request
-------

.. rest_parameters:: parameters.yaml

    - hosts: hosts
    - state: workflow-state
    - maintenance_at: maintenance-workflow-start-time
    - workflow: workflow-name
    - metadata: metadata
    - download: upgrade-list
    - actions: action-plugins
    - actions.plugin: action-plugin-name
    - actions.type: action-plugin-type
    - actions.metadata: action-metadata

.. literalinclude:: samples/create-maintenance-session-post.json
   :language: javascript

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: create-maintenance-session-post

.. rest_status_code:: error status.yaml

    - 400
    - 500
    - 509

Update maintenance session (planned future functionality)
=========================================================

.. rest_method:: PUT /v1/maintenance/{session_id}

Update existing maintenance session. This can be used to continue a failed
session after manually fixing what failed. Workflow should then run
succesfully to the end.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: session_id
    - state: workflow-state-optional

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: maintenance-session-put

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500

Get maintenance sessions
========================

.. rest_method:: GET /v1/maintenance

Get all ongoing maintenance sessions.

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: maintenance-sessions-get

.. rest_status_code:: error status.yaml

    - 400
    - 500

Get maintenance session
=======================

.. rest_method:: GET /v1/maintenance/{session_id}

Get a maintenance session state.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: session_id

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: maintenance-session-get

.. rest_status_code:: error status.yaml

    - 400
    - 404
    - 422
    - 500

Get maintenance session details
===============================

.. rest_method:: GET /v1/maintenance/{session_id}/detail

Get a maintenance session details. This information can be usefull to see
detailed status of a maintennace session or to troubleshoot a failed session.
Usually session should fail on simple problem, that can be fast manually
fixed. Then one can update maintenance session state to continue from 'prev_state'.

Request
-------

.. rest_parameters:: parameters.yaml

    - session_id: session_id

Response codes
--------------

.. rest_status_code:: success status.yaml

    - 200: maintenance-session-detail-get

.. rest_status_code:: error status.yaml

    - 400
    - 404
    - 422
    - 500

Delete maintenance session
==========================

.. rest_method:: DELETE /v1/maintenance/{session_id}

Delete a maintenance session. Usually called after the session is successfully
finished.


.. rest_status_code:: success status.yaml

    - 200

.. rest_status_code:: error status.yaml

    - 400
    - 422
    - 500
