# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

hacking>=2.0,<2.1 # Apache-2.0
coverage>=6.1.1 # Apache-2.0
python-subunit>=1.4.0 # Apache-2.0/BSD
oslotest>=4.5.0 # Apache-2.0
stestr>=3.2.1 # Apache-2.0
testtools>=2.5.0 # MIT
ddt>=1.4.4 # MIT
mock>=4.0.3 # BSD
fixtures>=3.0.0 # Apache-2.0/BSD
testrepository>=0.0.20 # Apache-2.0/BSD
testscenarios>=0.5.0 # Apache-2.0/BSD
oslo.context>=3.4.0 # Apache-2.0
oslo.config>=8.7.1  # Apache-2.0
oslo.log>=4.6.1  # Apache-2.0
oslo.db>=11.0.0  # Apache-2.0
oslo.policy>=3.9.0  # Apache-2.0
oslo.messaging>=12.11.0 # Apache-2.0
