# plugin.sh - DevStack plugin.sh dispatch script fenix

function install_fenix {
    git_clone $FENIX_REPO $FENIX_DIR $FENIX_BRANCH
    setup_develop $FENIX_DIR
}

function init_fenix {
    recreate_database fenix utf8
}

function configure_fenix {
    if [[ ! -d $FENIX_CONF_DIR ]]; then
        sudo mkdir -p $FENIX_CONF_DIR
    fi
    sudo chown $STACK_USER $FENIX_CONF_DIR
    touch $FENIX_CONF_FILE
    touch $FENIX_API_CONF_FILE

    create_service_user "$FENIX_USER_NAME" "admin"
    init_fenix_service_user_conf
    init_fenix_keystone

    iniset $FENIX_CONF_FILE DEFAULT host "$FENIX_SERVICE_HOST"
    iniset $FENIX_API_CONF_FILE DEFAULT host "$FENIX_SERVICE_HOST"
    iniset $FENIX_CONF_FILE DEFAULT port "$FENIX_SERVICE_PORT"
    iniset $FENIX_API_CONF_FILE DEFAULT port "$FENIX_SERVICE_PORT"
    iniset $FENIX_CONF_FILE DEFAULT transport_url $(get_notification_url)
    iniset $FENIX_API_CONF_FILE DEFAULT transport_url $(get_notification_url)

    iniset $FENIX_CONF_FILE database connection `database_connection_url fenix`
}

function init_fenix_service_user_conf {
    iniset $FENIX_CONF_FILE service_user os_auth_url "$KEYSTONE_SERVICE_URI"
    iniset $FENIX_CONF_FILE service_user os_username "$FENIX_USER_NAME"
    iniset $FENIX_CONF_FILE service_user os_password "$SERVICE_PASSWORD"
    iniset $FENIX_CONF_FILE service_user os_user_domain_name "$SERVICE_DOMAIN_NAME"
    iniset $FENIX_CONF_FILE service_user os_project_name "$SERVICE_TENANT_NAME"
    iniset $FENIX_CONF_FILE service_user os_project_domain_name "$SERVICE_DOMAIN_NAME"
}

function  init_fenix_keystone {
    iniset $FENIX_API_CONF_FILE keystone_authtoken cafile "$SSL_BUNDLE_FILE"
    iniset $FENIX_API_CONF_FILE keystone_authtoken project_domain_name "$SERVICE_DOMAIN_NAME"
    iniset $FENIX_API_CONF_FILE keystone_authtoken project_name "$SERVICE_TENANT_NAME"
    iniset $FENIX_API_CONF_FILE keystone_authtoken user_domain_name "$SERVICE_DOMAIN_NAME"
    iniset $FENIX_API_CONF_FILE keystone_authtoken password "$SERVICE_PASSWORD"
    iniset $FENIX_API_CONF_FILE keystone_authtoken username "$FENIX_USER_NAME"
    iniset $FENIX_API_CONF_FILE keystone_authtoken auth_url "$KEYSTONE_SERVICE_URI"
    iniset $FENIX_API_CONF_FILE keystone_authtoken auth_type "password"
}

function create_fenix_accounts {
    SERVICE_TENANT=$(openstack project list | awk "/ $SERVICE_TENANT_NAME / { print \$2 }")
    ADMIN_ROLE=$(openstack role list | awk "/ admin / { print \$2 }")

    FENIX_USER_ID=$(get_or_create_user $FENIX_USER_NAME \
        "$SERVICE_PASSWORD" "default" "fenix@example.com")
    get_or_add_user_project_role $ADMIN_ROLE $FENIX_USER_ID $SERVICE_TENANT

    fenix_api_url="$FENIX_SERVICE_PROTOCOL://$FENIX_SERVICE_HOST:$FENIX_SERVICE_PORT"

    FENIX_SERVICE=$(get_or_create_service "fenix" \
        "maintenance" "Fenix maintenance Service")
    get_or_create_endpoint $FENIX_SERVICE \
        "$REGION_NAME" \
        "$fenix_api_url/v1"

    # Create admin and internal endpoints for keystone. Fenix currently uses
    # the admin endpoint to interact with keystone, but devstack stopped
    # creating one in https://review.opendev.org/c/openstack/devstack/+/777345
    KEYSTONE_SERVICE=$(get_or_create_service "keystone" \
        "identity" "Keystone Identity Service")
    get_or_create_endpoint $KEYSTONE_SERVICE \
        "$REGION_NAME" \
        "${KEYSTONE_SERVICE_PROTOCOL}://${KEYSTONE_SERVICE_HOST}/identity" \
        "${KEYSTONE_SERVICE_PROTOCOL}://${KEYSTONE_SERVICE_HOST}/identity" \
        "${KEYSTONE_SERVICE_PROTOCOL}://${KEYSTONE_SERVICE_HOST}/identity"
}

function start_fenix {
    run_process fenix-api "$FENIX_BIN_DIR/fenix-api --debug --config-file $FENIX_API_CONF_FILE"
    run_process fenix-engine "$FENIX_BIN_DIR/fenix-engine --debug --config-file $FENIX_CONF_FILE"
}

function stop_fenix {
    for serv in fenix-api fenix-engine; do
        stop_process $serv
    done
}

# check for service enabled
if is_service_enabled fenix-api && is_service_enabled fenix-engine; then

    if [[ "$1" == "stack" && "$2" == "pre-install" ]]; then
        # Set up system services
        echo_summary "Pre installation for Fenix"

    elif [[ "$1" == "stack" && "$2" == "install" ]]; then
        # Perform installation of service source
        echo_summary "Installing Fenix"
        install_fenix

    elif [[ "$1" == "stack" && "$2" == "post-config" ]]; then
        # Configure after the other layer 1 and 2 services have been configured
        echo_summary "Configuring Fenix"
        configure_fenix
        create_fenix_accounts

    elif [[ "$1" == "stack" && "$2" == "extra" ]]; then
        # Initialize and start the fenix service
        echo_summary "Initializing Fenix"
        init_fenix
        start_fenix
    fi

    if [[ "$1" == "unstack" ]]; then
        # Shut down fenix services
        # no-op
        stop_fenix
    fi

fi
